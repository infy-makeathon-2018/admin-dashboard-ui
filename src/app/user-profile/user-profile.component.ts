import { Component, OnInit } from '@angular/core';


declare interface UserInfo {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
}

export const USERDETAILS: UserInfo[] = [
  {username: 'madhu_aithal', firstName: 'Madhusudhan', lastName: 'Aithal', email: 'madhu@email.com'},
  {username: 'dharam_unadkat', firstName: 'Dharam', lastName: 'Unadkat', email: 'dharam@email.com'}
];

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userItems: any[];

  constructor() { }

  ngOnInit() {
    this.userItems = USERDETAILS.filter(userItem => userItem);

  }

}
